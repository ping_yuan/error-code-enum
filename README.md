# 错误码枚举类
使用注释优雅地维护错误码，错误信息的枚举类。

### 背景
枚举类型在开发过程中有广泛应用，本人之前整理一个[枚举类](https://gitee.com/ping_yuan/error-code-enum)，主要应用场景是表示字段状态的枚举，比如：一个活动状态，订单状态等。
在接口开发中，状态码和错误信息的维护往往是一项头痛的事情，过去我们团队是另外文本记录所有错误信息的字典，程序员开发时，要对照着字典一个个找，这显然效率非常低的。
其实，使用枚举类也能优雅地对错误码，错误信息进行优雅地维护管理。参考hyperf框架的[hyperf/constants](https://github.com/hyperf/constants)，整理一个专门用于管理错误码，错误信息的枚举类。

### 好处
- 代码层限制变量的取值范围，提高代码维护性。每一个状态码相当于一个类的常量，不同的程序员重复或者误定义错误码。
``` PHP
// 定义一个用户权限相关的错误码枚举类
class UserAuthCode extends ErrorCode{
    /***     
    * @Message("登录超时")     
    */
    const TIME_OUT = 20001;
    /***     
    * @Message("登录密码错误")     
    */
    const PWD_ERROR = 20002;
    /***     
    * @Message("账号%s不存在")     
    */
    const USERNAME_NOT_EXIST = 20001;
}
```
- 对IDE（测试使用phpstorm）友好，不需要额外的文本记录错误码字典，提高开发效率。

![输入图片说明](https://res.dkou.top/dkou_img_url202202071117492403)

### 安装
```
composer install dkou/error-code-enum
```
### 定义
直接继承，根据不同业务定义不同的错误码枚举类以下为使用例子：
```PHP
// 定义一个用户权限相关的错误码枚举类
class UserAuthCode extends ErrorCode{
    /***     
    * @Message("登录超时")     
    */
    const TIME_OUT = 20001;
    /***     
    * @Message("登录密码错误")     
    */
    const PWD_ERROR = 20002;
    /***     
    * @Message("账号%s不存在")     
    */
    const USERNAME_NOT_EXIST = 20001;
}
```
#### 状态码
- 命名只能是大写英文字母+下划线组合。
#### 错误信息
- 注释格式必须与案例一致.其中Message必须M大写，错误信息必须使用双引号包裹。
- %s为占位符。

### 使用
#### 获取错误码
```PHP
UserAuthCode::TIME_OUT;
```
#### 获取错误码的错误信息
```PHP
// 直接获取
UserAuthCode::getMessage(UserAuthCode::TIME_OUT);
// 错误信息带占位符时，可加入上下文信息
UserAuthCode::getMessage(UserAuthCode::USERNAME_NOT_EXIST,['xxxxx']);
```
#### 常用的接口返回格式返回
一般是接口返回结果的格式，都是一个数组里面包含code,message,data。
```PHP
// 直接获取
UserAuthCode::compser(UserAuthCode::TIME_OUT);
// 拼接上下文data数据
$data = [];
UserAuthCode::getMessage(UserAuthCode::USERNAME_NOT_EXIST, ['xxxxx'], $data);
```