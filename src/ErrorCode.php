<?php

namespace dkou\errorCodeEnum;

use ReflectionClass;
use dkou\errorCodeEnum\exceptions\ErrorCodeException;

/**
 * 错误码枚举类
 * Class ErrorCode
 */
class ErrorCode
{
    /**
     * @Message("已发布22222")
     */

    /**
     * @var array 
     */
    public static $mapping = [];

    /**
     * 数据初始化
     * @param $code
     * @throws ErrorCodeException
     */
    public static function init()
    {
        $class = get_called_class();
        if(!isset(static::$mapping[$class])){
            // 通过注释获取数据
            $ref = new \ReflectionClass(static::class);
            $classConstants = $ref->getReflectionConstants();
            $Reader = new AnnotationReader();
            $result = $Reader->getAnnotations($classConstants);
            if(empty($result)){
                throw new ErrorCodeException('Mapping is empty');
            }
            self::$mapping[$class] = $result;
        }
    }

    /**
     * 获取提示信息文本
     * @param $code
     * @param array $context
     * @return string
     * @throws ErrorCodeException
     */
    public static function getMessage($code, $context = [])
    {
        self::init();
        $class = get_called_class();
        if(isset(self::$mapping[$class][$code]['message'])){
            $message = empty($context) ? sprintf(self::$mapping[$class][$code]['message'], '') : sprintf(self::$mapping[$class][$code]['message'], ...(array) $context);
            return $message;
        }else{
            return '';
        }
    }

    /**
     * 组合输出
     * @param $code
     * @param array $context
     * @return array
     * @throws ErrorCodeException
     */
    public static function compose($code, $context = [], $data = [])
    {
        $data['message'] = self::getMessage($code, $context);
        $data['code'] = $code;
        return $data;
    }

}